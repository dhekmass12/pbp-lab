from django.db import models
from lab_2.models import Note
from django import forms

class NoteForm(forms.ModelForm):
    class Meta:
        model = Note
        fields = ['to', 'from_who', 'title', 'message']
    error_messages = {
        'required': 'Please Type'
    }
    input_attrs = {
        'type' : 'text',
        'placeholder':'data kamu'
    }
    to = forms.CharField(label = 'To', required=False, max_length=30, widget=forms.TextInput(attrs=input_attrs))
    from_who = forms.CharField(label = 'From', required=False, max_length=30, widget=forms.TextInput(attrs=input_attrs))
    title = forms.CharField(label = 'Title', required=False, max_length=30, widget=forms.TextInput(attrs=input_attrs))
    message = forms.CharField(label = 'Message', required=False, max_length=30, widget=forms.TextInput(attrs=input_attrs))