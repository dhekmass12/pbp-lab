1. Perbedaan antara JSON dan XML adalah:
	- JSON mengirimkan data yang berbentuk dictionary sedangkan xml mengirim data dalam bentuk elemen-elemen
	- Syntax pada JSON mirip seperti JavaScript sedangkan xml mirip seperti html
2. Perbedaan antara HTML dan XML adalah:
	- HTML terdapat hypertext sedangkan XML tidak
	- HTML digunakan untuk membuat tampilan website sedangkan XML hanya digunakan untuk mengirim data
