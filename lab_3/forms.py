from django.db import models
from lab_1.models import Friend
from django import forms

class FriendForm(forms.ModelForm):
    class Meta:
        model = Friend
        fields = ['name', 'npm', 'dob']
    error_messages = {
        'required': 'Please Type'
    }
    input_attrs = {
        'type' : 'text',
        'placeholder':'data kamu'
    }
    name = forms.CharField(label = 'Name', required=False, max_length=30, widget=forms.TextInput(attrs=input_attrs))
    npm = forms.CharField(label = 'Npm', required=False, max_length=30, widget=forms.TextInput(attrs=input_attrs))
    dob = forms.CharField(label = 'DOB', required=False, max_length=30, widget=forms.TextInput(attrs=input_attrs))